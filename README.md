# PlaneGame

#### 介绍
基于SDL2实现的飞机大战小游戏

#### 编译说明
1. 安装SDL2、CMake
2. 将```simkai.ttf```从```ttf.zip```提取出来
3. 编译
    ```shell
    cmake ./
    make
    ```