#include "planeGame.hpp"

int Main(int argc, char *argv[]) {
	PlaneGame game;
	game.init();
	game.mainLoop();
	
	return 0;
}